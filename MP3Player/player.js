//当前播放器对象
var aobo_curObj = null;
//状态间隔
var aobo_stateInterval = null;
//等待间隔
var aobo_waitInterval = null;
//REAL事件间隔
var aobo_aobo_realStateInterval = null;
//全局声音
var aobo_globalVol = 100;
//
var aobo_realEnable = true;
var aobo_mediaEnable = true;
//REAL状态
var aobo_realState = -1;
//用于判断停止事件
var aobo_stopBool = false;
//MEDIA和REAL的播放类型
var aobo_mediaType = ".mpg;.mpeg;.mpe;.mp2;.mp3;.avi;.wmv;.dat;.asf;.mid;.midi;.wav;.wma;.aif;.aifc;.aiff;.au;";
var aobo_realType = ".rmj;.rmvb;.ra;.rm;.ram;";
//判断浏览器是否基于IE内核
var aobo_explorerEnable = false;
if (navigator.appName.indexOf("Microsoft") !=-1) {
       aobo_explorerEnable = true;
}else{
}
//发送指令给FLASH加入音乐文件
function addMusic(s) {
    aobo_thisMovie("miniplayer").addNewMusic(s);
}
//获取FLASH对象
function aobo_thisMovie(movieName) {
    if (navigator.appName.indexOf("Microsoft") !=-1) {
        return window[movieName];
    }
    else {
        return document[movieName];
    }
}
//------------------------------
function setFLashStageWidth(bool) {

	//将请求地址传给Flash
	addMusic("musiclist.xml");

	aobo_thisMovie("miniplayer").addLRCCSS("FlashMp3Player.css");
} 
function setDiv(bool){
	if(bool){
		document.getElementById("Javascript.DivPlayer").style.height = "156px";
	}else{
		document.getElementById("Javascript.DivPlayer").style.height = "340px";
	}	
}
//-----------------------------
//写MEDIA
function outputMedia(){
	document.writeln('<object classid="clsid:6BF52A52-394A-11D3-B153-00C04F79FAA6" id="mediaObj"');
	document.writeln('  width="100%" height="100%" onerror="MediaPlayerOnError()"');
	document.writeln('    <param name="rate" value="-1">');
	document.writeln('    <param name="balance" value="0">');
	document.writeln('    <param name="currentPosition" value="0">');
	document.writeln('    <param name="playCount" value="1">');
	document.writeln('    <param name="autoStart" value="-1">');
	document.writeln('    <param name="currentMarker" value="0">');
	document.writeln('    <param name="invokeURLs" value="0">');
	document.writeln('    <param name="volume" value="80">');
	document.writeln('    <param name="mute" value="0">');
	document.writeln('    <param name="uiMode" value="full">');
	document.writeln('    <param name="stretchToFit" value="-1">');
	document.writeln('    <param name="windowlessVideo" value="0">');
	document.writeln('    <param name="enabled" value="-1">');
	document.writeln('    <param name="enableContextMenu" value="0">');
	document.writeln('    <param name="fullScreen" value="0">');
	document.writeln('    <param name="enableErrorDialogs" value="0">');
	document.writeln('  </object>');
}
//写REAL
function outputReal(){
	
	document.writeln('<object TYPE="audio/x-pn-realaudio-plugin" name="realObj" id="realObj"');
	document.writeln('  width="100%" height="100%" onerror="realOnError()">');
	document.writeln('    <param name="AUTOSTART" value="-1">');
	document.writeln('    <param name="SHUFFLE" value="0">');
	document.writeln('    <param name="PREFETCH" value="0">');
	document.writeln('    <param name="NOLABELS" value="-1">');
	document.writeln('    <param name="CONTROLS" value="ControlPanel,StatusBar">');
	document.writeln('    <param name="LOOP" value="0">');
	document.writeln('    <param name="NUMLOOP" value="0">');
	document.writeln('    <param name="CENTER" value="0">');
	document.writeln('    <param name="MAINTAINASPECT" value="0">');
	document.writeln('  </object>');
}
//判断播放文件的类型
function chkType(_s,url){
	//初始化
	trace("文件类型:"+_s);
	trace("文件地址:"+url);
	if(aobo_curObj!=null && aobo_curObj!=undefined){
		aobo_curObj.stop();
	}
	aobo_curObj = null;
	aobo_stopBool=false;
	clearInterval(aobo_aobo_realStateInterval);
	clearInterval(aobo_waitInterval);
	if(!aobo_explorerEnable||_s==undefined){
		return;
	}
	var tp = "none";
	if(aobo_mediaType.indexOf("."+_s+";")!= -1){
		tp = "media";
	}else if(aobo_realType.indexOf("."+_s+";")!=-1){
		tp = "real";
	}
	switch(tp){
		case "media":
			if(aobo_mediaEnable){
				aobo_curObj =  new MediaPlayer;
				aobo_curObj.open(url);
				aobo_curObj.volume(aobo_globalVol);
				aobo_waitInterval = setInterval("aobo_waiting()",10000);
				break;
			}else{
				broadcastEvent({op:"skip"});
			}
		case "real":
			if(aobo_realEnable){
				aobo_curObj = new RealPlayer;
				aobo_curObj.open(url);
				aobo_curObj.volume(aobo_globalVol);
				aobo_waitInterval = setInterval("aobo_waiting()",10000);
				break;
			}else{
				broadcastEvent({op:"skip"});
			}
		case "none" :
			//未找到可以播放的类型
			broadcastEvent({op:"skip"});
			break;
		default:
	}
	//
	aobo_curObj.volume(gobalVol);
}
function aobo_waiting(){
	aobo_curObj.stop();
	clearInterval(aobo_stateInterval);
	clearInterval(aobo_waitInterval);
	broadcastEvent({op:"skip"});
}
//声明MediaPlayer类
function MediaPlayer(){
	//初始化 WindowsMedia播放器
	this.aobo_obj = null;
	this.aobo_obj = document.mediaObj;
	if(this.aobo_obj==null){
		trace("null");
	}
	//打开地址
	this.open = function(url){
		this.aobo_obj.URL = url;
		//this.stop();
		//this.play();
		trace("playing");
	}
	this.play = function(){this.aobo_obj.controls.play();}
	this.pause = function(){this.aobo_obj.controls.pause();}
	this.stop = function(){
		this.aobo_obj.controls.stop();
		//this.aobo_obj.controls.currentPosition = 0;
	}
	this.go = function(s){trace(s);this.aobo_obj.controls.currentPosition = s;}
	this.pos = function(){return this.aobo_obj.controls.currentPosition;}
	this.length = function(){return this.aobo_obj.currentMedia.duration;}
	this.state = function(){
		var ps = this.aobo_obj.PlayState;
		return ps;
	}
	this.volume = function(s){
		this.aobo_obj.settings.volume = s;
	}
	this.mute = function(s){ 
		this.aobo_obj.settings.mute = s;
	}	
}
//MEDIA错误处理
function MediaPlayerOnError(){
	//alert("MediaPlayerOnError");
	if(aobo_curObj==null || aobo_curObj.constructor != MediaPlayer){
		alert("找不到Media Player，请先安装Media Player 9.0或其以上版本");
		aobo_mediaEnable = false;
		return;
	}
	clearInterval(aobo_stateInterval);
	clearInterval(aobo_waitInterval);
	broadcastEvent({op:"skip"});
}
//MEDIA状态
function MediaPlayerState(state){
	switch  (state) {
        case  1:
			   trace("停止");
			   //updatetime(aobo_curObj.pos());
			   clearInterval(aobo_stateInterval);
			   trace("media aobo_stopBool:"+aobo_stopBool);
			   if(aobo_stopBool){
					broadcastEvent({op:"stop"});
			   }
               break;
        case  2:
				trace("暂停");
				clearInterval(aobo_stateInterval);
				//broadcastEvent({op:"pause"});
                break;
        case  3:
				trace("正在播放");
				clearInterval(aobo_stateInterval);
				aobo_stateInterval = setInterval("updatetime(aobo_curObj.pos())",100);
				broadcastEvent({op:"play"});
				clearInterval(aobo_waitInterval);
                break;
        case  6:
				//trace("正在缓冲...");
				clearInterval(aobo_stateInterval);
				broadcastEvent({op:"buffer"});
				clearInterval(aobo_waitInterval);
				//showBufferPercent();
				trace("缓冲...");
                break;
        case  9:
				//trace("正在连接...");
				clearInterval(aobo_stateInterval);
				broadcastEvent({op:"connect"});
                break;
        case  10:
				//trace("准备就绪");
                break;
        default: 
	} 
}
//声明REAL对象
function RealPlayer(){
	//初始化 Real播放器
	this.aobo_obj = null;
	this.aobo_obj = document.realObj;
	if(this.aobo_obj==null){
		trace("null");
	}
	//打开地址
	this.open = function(url){
		this.aobo_obj.SetSource(url);
		this.play();
		aobo_aobo_realStateInterval = setInterval("getRealState()",100);
	}
	this.play = function(){
		//if(this.aobo_obj.CanPlay()){
			this.stop();
			this.aobo_obj.DoPlay();
		//}
	}
	this.pause = function(){this.aobo_obj.DoPause();}
	this.stop = function(){
		if(this.aobo_obj.CanStop()){
			this.aobo_obj.DoStop();
			//this.aobo_obj.SetPosition(0);
		}
	}
	this.go = function(s){this.aobo_obj.SetPosition(s*1000);}
	this.pos = function(){return this.aobo_obj.GetPosition()/1000;}
	this.length = function(){return this.aobo_obj.GetLength()/1000;}
	this.state = function(){return this.aobo_obj.GetPlayState();}
	this.volume = function(s){this.aobo_obj.SetVolume(s);}
	this.mute = function(s){this.aobo_obj.SetMute(s);}
}
//REAL错误处理
function realOnError(){
	//alert("realOnError");
	if(aobo_curObj==null || aobo_curObj.constructor != RealPlayer){
		//alert("找不到Real Player，请安装Real Player 8.0以上版本");
		aobo_realEnable = false;
		return;
	}
	clearInterval(aobo_stateInterval);
	clearInterval(aobo_waitInterval);
	broadcastEvent({op:"skip"});
}
//REAL状态
function RealPlayerState(state){
	switch  (state) {
        case  0:
			   trace("Real停止");
			   clearInterval(aobo_stateInterval);
			   trace("real aobo_stopBool:"+aobo_stopBool);
			   if(aobo_stopBool){
					broadcastEvent({op:"stop"});
			   }
               break;
        case  4:
				trace("Real暂停");
				clearInterval(aobo_stateInterval);
                break;
        case  3:
				trace("Real正在播放");
				clearInterval(aobo_stateInterval);
				aobo_stateInterval = setInterval("updatetime(aobo_curObj.pos())",100);
				broadcastEvent({op:"play"});
				clearInterval(aobo_waitInterval);
                break;
        case  2:
				//trace("Real正在缓冲...");
				clearInterval(aobo_stateInterval);
				broadcastEvent({op:"buffer"});
				clearInterval(aobo_waitInterval);
                break;
        case  1:
				trace("Real正在连接...");
				clearInterval(aobo_stateInterval);
				broadcastEvent({op:"connect"});
                break;
        default: 
	}
}
//获取REAL状态
function getRealState(){
	if(aobo_curObj==null || aobo_curObj==undefined){
		return;
	}
	if(aobo_realState!=aobo_curObj.state()){
		aobo_realState = aobo_curObj.state();
		RealPlayerState(aobo_realState);
	}
}
//传给FLASH当前播放时间和总时间
function updatetime(_n){
	if(aobo_curObj==null || aobo_curObj==undefined){
		return;
	}
	//trace(_n);
	if(_n>=1 && aobo_curObj.length()>1 && (aobo_curObj.length()-_n)<=1){
		aobo_stopBool=true;
	}
	broadcastEvent({op:"total",num:aobo_curObj.length()});
	broadcastEvent({op:"pos",num:_n});
}
//发送信息给FLASH
function broadcastEvent(_event){
	//给Flash传值
	aobo_thisMovie("miniplayer").getMsgFromJS(_event);
}
//从FLASH获取控制信息
function setMusicState(obj){
	trace("接收到的命令:"+obj.state);
	if(obj.state=="ns"){
		trace(obj.bool);
	}
	if(obj.state=="testIE"){
		trace("命令:"+obj.state);
		broadcastEvent({op:"ns", bool:aobo_explorerEnable});
	}
	if(obj.state=="setvol"){
		aobo_globalVol = obj.num;
		trace(aobo_globalVol);
	}
	if (!aobo_explorerEnable||aobo_curObj==null || aobo_curObj == undefined) {
		return;
	}
	switch(obj.state){
		case "play":
			aobo_curObj.play();
			break;
		case "pause":
			aobo_curObj.pause();
			break;
		case "stop":
			aobo_curObj.stop();
			break;
		case "setvol":
			aobo_curObj.volume(obj.num);
			break;
		case "setmute":
			aobo_curObj.mute(obj.bool);
			break;
		case "setpos":
			aobo_curObj.go(obj.num);
			break;	
		case "trace":
			trace("***"+obj.str+"***");
		default:
	}
}
//从FLASH获取播放地址
function setMusicURL(_s,url){
	trace("从FLASH获取播放地址");
	if (aobo_explorerEnable) {
       chkType(_s,url);
    }else {
		trace("略过");
		broadcastEvent({op:"skip"});
    }
}
//调试用方法
function trace(_s){
	//document.getElementById("out_put").value += _s+"\n";
	//document.getElementById("out_put").scrollTop = document.getElementById("out_put").scrollHeight;
}
//清除调试用文本内容
function clearTxt(){
	//document.getElementById("out_put").value = "";
}

//长条播放器设置-------
//本播放器版权归 www.aobo.com 所有
//随风行   2007-04-26 
//http://www.cuiz.net
var newwindow;
var child_array;
var select_num;
//点击FLASH中的列表图标时，会调用openList函数，打开新页面，并传值
function openList(list_array, num){

	child_array=list_array;
	select_num=num;
	if(child_array==undefined){child_array=new Array();}
	if(select_num==undefined){select_num=0;}
	newwindow.focus();
}
//接收来自新页面(列表面面)的数值，通知FLASH播放的歌曲项
function selecteCell(num){
	aobo_thisMovie("miniplayer").selectMusic(num);
}
//
function getArrayData(){
	return {a:child_array, n:select_num};
}